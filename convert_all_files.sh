#  This bash script automatically converts pdf files into 
#  txt files. 
#In order to execute this script, you have to: 
#  1 - Change the directory to your target directory (or leave as it is 
#      if you want to convert the files in the same directory of your bash file). 
#
#  2 - Install Python and the pdf-to-text module 
#      at the address https://github.com/euske/pdfminer . 
#
#  3 - Run chmod u+x convert_all_files.sh into the terminal 
#
#  4 - Run the script 
# 

#  !/bin/bash 

for file in *.pdf; do 

pdf2txt.py $file > "${file%.pdf}".txt

done 