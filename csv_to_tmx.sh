# This bash script converts a .csv file into a .tmx file. TMX files are used 
# to train translators such as Watson Translator. 
#
# The converter creates a forced glossary of translations 
# from italian to english. 
#
#    HOW TO USE 
#
#    1) Go to the terminal in the folder where this file is located. 
#
#    2) Run the command chmod u+x create_tmx.sh 
# 
#    3) Run the command ./create_tmx.sh <file_to_convert>, where file_to_convert 
#       is the name of the file that you want to convert 
# 
# Have fun! 
# 
# By Guido Rocco, IBM 
#



#!/bin/bash 

echo -e "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n" >> tmxfile.tmx 
echo -e "<tmx version=\"1.4\">\n\n" >> tmxfile.tmx 
echo -e "<header creationtool=\"\" creationtoolversion=\"\" \n
         segtype=\"phrase\" o-tmf=\"\" adminlang=\"it\" \n 
         srclang=\"it\" datatype=\"PlainText\" o-encoding=\"UTF-8\" /> \n\n\n " >> tmxfile.tmx 
echo "<body>\n" >> tmxfile.tmx 


while IFS=";" read -r first last 
do 
   echo -e "<tu>" >> tmxfile.tmx 
   echo -e "<tuv xml:lang=\"it\"> \n" >> tmxfile.tmx 
   echo -e "   <seg> $first </seg>\n" >> tmxfile.tmx 
   echo -e "</tuv>\n" >> tmxfile.tmx 
   echo -e "<tuv xml:lang=\"en\">\n" >> tmxfile.tmx 
   echo -e "   <seg> $last </seg>\n" >> tmxfile.tmx 
   echo -e "</tuv>" >> tmxfile.tmx 
   echo -e "</tu>\n" >> tmxfile.tmx 
done < "$1" 

echo -e "</body> \n\n</tmx>" >> tmxfile.tmx 

echo "Done! Conversion has been made" 

exit 
